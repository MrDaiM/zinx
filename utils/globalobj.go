package utils

import (
	"encoding/json"
	"fmt"
	"gitee.com/MrDaiM/zinx/ziface"
	"io/ioutil"
	"os"
)

/*
存储一些有关Zinx框架的全局参数，功其他模块使用
一些参数也可以通过 用户根据 zinx.json 来配置
*/
type GlobalObj struct {

	/*
		Server
	*/
	// 当前Zinx的全局Server对象
	TcpServer ziface.IServer
	// 当前服务器主机IP
	Host string
	// 当前服务器主机监听端口
	TcpPort int
	// 当前服务器名称
	Name string

	/*
		Zinx
	*/
	// 当前Zinx版本号
	Version string
	// 需要数据包的最大值
	MaxPacketSize uint32
	// 当前服务器主机允许的最大链接数
	MaxCount int
	// 业务工作Worker池的数量
	WorkerPoolSize uint32
	// 业务工作Worker对应负责的任务队列最大任存储数量
	MaxWorkTaskLen uint32
	/*
		config file path
	*/
	ConFilePath string
	// 有缓冲的读写消息通信容量
	MaxMsgChanLen int
	// 最大链接数
	MaxConn int
}

// PathExists 判断一个文件是否存在
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err != nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// Reload 读取配置文件
func (g *GlobalObj) Reload() {

	if confFileExists, _ := PathExists(g.ConFilePath); confFileExists != true {
		return
	}

	data, err := ioutil.ReadFile(GlobalObject.ConFilePath)
	if err != nil {
		panic(err.(interface{}))
	}
	// 将json的数据解析到struct中
	fmt.Printf("json： %s \n", data)
	err = json.Unmarshal(data, &GlobalObject)
	if err != nil {
		panic(err.(any))
	}
}

// GlobalObject 定义一个全局的对象
var GlobalObject *GlobalObj

// 提供init方法 默认加载  在main函数调用之前，初始化一些值
func init() {

	pwd, err := os.Getwd()
	if err != nil {
		pwd = "."
	}

	// 初始化 GlobalObject 变量， 设置一些默认值
	GlobalObject = &GlobalObj{
		Name:           "ZinxServerApp",
		Version:        "V0.4",
		TcpPort:        7777,
		Host:           "0.0.0.0",
		MaxCount:       12000,
		MaxPacketSize:  4096,
		ConFilePath:    pwd + "/conf/zinx.json",
		WorkerPoolSize: 10,
		MaxWorkTaskLen: 1024,
		MaxConn:        100,
		MaxMsgChanLen:  10,
	}
	// 从配置文件中加载一些用户配置参数
	GlobalObject.Reload()

}
