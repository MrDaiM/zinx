package znet

import (
	"bytes"
	"encoding/binary"
	"errors"
	"gitee.com/MrDaiM/zinx/utils"
	"gitee.com/MrDaiM/zinx/ziface"
)

// DataPack 封包拆包类实例， 暂时不需要成员
type DataPack struct {}


// NewDataPack 封包拆包实例初始化方法
func NewDataPack() *DataPack {
	return &DataPack{}
}

// GetHeadLen 消息头部长度
func(dp *DataPack) GetHeadLen() uint32 {
	// Id uint32(4字节) + DataLen uint32(4字节)
	return 8
}
// Pack 封包方法(压缩数据)
func(dp *DataPack) Pack(msg ziface.IMessage)([]byte, error) {

	// 创建一个存放bytes字节的缓冲
	dataBuff := bytes.NewBuffer([]byte{})

	// 写dataLen
	if err := binary.Write(dataBuff, binary.LittleEndian, msg.GetDataLen()); err != nil {
		return nil, err
	}

	// 写msgID
	if err := binary.Write(dataBuff, binary.LittleEndian, msg.GetMsgId()); err != nil {
		return nil, err
	}

	// 写data数据
	if err := binary.Write(dataBuff, binary.LittleEndian, msg.GetData()); err != nil {
		return nil,err
	}

	return dataBuff.Bytes(), nil


}
// Unpack 拆包方法 (解压数据)
func(dp *DataPack) Unpack(binaryData []byte)(ziface.IMessage, error) {

	// 创建一个从输入二进制数据的ioReader
	dataBuffer := bytes.NewReader(binaryData)

	// 直解压head的信息，得到dataLen和msgId
	msg := &Message{}

	// 读dataLen
	// DataLen 是 uint类型 4字节 这个方法 Read 会读取四字节长度
	if err := binary.Read(dataBuffer, binary.LittleEndian, &msg.DataLen); err != nil {
		return nil, err
	}

	// 读msgID
	// Id  是 uint类型 4字节 这个方法 Read 会读取四字节长度
	if err := binary.Read(dataBuffer, binary.LittleEndian, &msg.Id); err != nil {
		return nil, err
	}

	// 判断dataLen的长度是否超出我们允许的最大包长度
	if utils.GlobalObject.MaxPacketSize > 0 && msg.DataLen > utils.GlobalObject.MaxPacketSize {
		return nil, errors.New("Too large msg data receive ")
	}
	// 这里只需要把head的数据拆包出来就可以了，然后在通过head的长度，在从conn读取一次数据
	return msg, nil

}