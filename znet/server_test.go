package znet

import (
	"fmt"
	"gitee.com/MrDaiM/zinx/ziface"
	"net"
	"testing"
	"time"
)


/*
	模拟客户端
*/
func ClientTest() {
	fmt.Println("Client Test ... start")
	// 3s秒之后发起测试，给服务端开启服务的机会
	time.Sleep(3 * time.Second)

	conn, err := net.Dial("tcp","127.0.0.1:7779")
	if err != nil {
		fmt.Println("client start err, exit!")
		return
	}

	// 循环发送以及监听读取
	for  {

		// 向服务器发送数据
	 	_, err := conn.Write([]byte("hello world"))
		if err != nil {
			 fmt.Println("write error err ", err)
			return
		}
		// 监听服务器发送的数据
		buf := make([]byte, 512)
		cnt, err := conn.Read(buf)
		if err != nil {
			fmt.Println("read buf error ", err)
			return
		}
		fmt.Printf(" server call back:%s, cnt = %d\n",buf, cnt)

		// 减缓处理速度
		time.Sleep(1 * time.Second)
	}


}

// ping test 自定义理由
type PingRouter struct {
	BaseRouter // 一定要先继承BaseRouter
}



// Server 模块的测试函数
func TestServer(t *testing.T) {

	/*
		服务端测试
	*/
	// 1. 创建一个Server 句柄 s
	s := NewServer()

	// 添加路由
	s.AddRouter(0,&PingRouter{})


	/*
		客户测试
	*/
	go ClientTest()

	// 2. 开启服务
	s.Serve()



}



// 实现路由提供的方法
// PreHandle Test PreHandle
func (br *PingRouter)PreHandle(request ziface.IRequest) {
	fmt.Println("Call Router PreHandle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("before ping ...\n"))
	if err != nil  {
		fmt.Println("Call back ping ping ping err")
	}

}
// Handle Test Handle
func (br *PingRouter) Handle(request ziface.IRequest){

	fmt.Println("Call PingRouter Handle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("ping...ping...ping\n"))
	if err != nil {
		 fmt.Println("call back ping ping ping err")
	}


}
// PostHandle Test PostHandle
func (br *PingRouter) PostHandle(request ziface.IRequest){
	fmt.Println("Call Router PostHandle")
	_, err := request.GetConnection().GetTCPConnection().Write([]byte("After ping ... \n"))
	if err != nil {
		fmt.Println("call back after ping err")
	}

}
