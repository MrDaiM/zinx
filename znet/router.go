package znet

import "gitee.com/MrDaiM/zinx/ziface"

// BaseRouter 实现router时，先嵌入这个基类，然后根据需要对这个基类的方法进行重写
type BaseRouter struct {}


// ==================
 /**
 	这里的基类的方法都是未实现的
 	之所以BaseRouter的方法都为空
 	是应为有Router不希望有PreHandle 活  PostHandle
    所以Router全部集成BaseRouter的好处是，不需要实现PreHandle 活  PostHandle 也可以实例化
  */
// ==================

// PreHandle 在处理conn业务之前的钩子方法
func (br *BaseRouter)PreHandle(request ziface.IRequest) {

}
// Handle 处理conn业务的方法
func (br *BaseRouter) Handle(request ziface.IRequest){

}
// PostHandle 处理conn业务之后的钩子方法
func (br *BaseRouter) PostHandle(request ziface.IRequest){

}
