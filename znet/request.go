package znet

import "gitee.com/MrDaiM/zinx/ziface"

type Request struct {
	// conn 已经和客户端建立好的连接
	conn ziface.IConnection
	// data 客户端请求的数据
	// data []byte 替换成封装的IMessage
	// 客户端请求的数据
	msg ziface.IMessage
}
// GetConnection 获取请求连接信息
func(request *Request) GetConnection() ziface.IConnection {
	return request.conn
}
// GetData 获取请求消息的数据
func(request *Request) GetData() []byte {
	return  request.msg.GetData()
}

// GetMsgID 获取请消息的ID
func(request *Request) GetMsgID() uint32 {
	return request.msg.GetMsgId()
}