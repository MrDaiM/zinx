package ziface

/*
	IRouter 路由接口， 这里面路由是 使用框架者给连接自定义的 处理业务方法
	路由里面的IRequest 则包含用该链接的 连接信息 和该 链接的请求数据信息
*/
type IRouter interface {
	// PreHandle 在处理conn业务之前的钩子方法
	PreHandle(request IRequest)
	// Handle 处理conn业务的方法
	Handle(request IRequest)
	// PostHandle 处理conn业务之后的钩子方法
	PostHandle(request IRequest)
}
