package ziface

/*
	IRequest 接口：
	实际上是白客户端请求的 连接信息 和 请求信息 包装到Request里
*/
type IRequest interface {
	// GetConnection 获取请求连接信息
	GetConnection() IConnection
	// GetData 获取请求消息的数据
	GetData() []byte
	// GetMsgID 获取消息Id
	GetMsgID() uint32
}
