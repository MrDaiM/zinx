package ziface

import "net"

// IConnection 定义连接接口
type IConnection interface {

	// Start 启动连接，让当前连接开始工作
	Start()
	// Stop 停止连接，结束当前连接状态
	Stop()
	// GetTCPConnection 从当前连接获取原始的socket TCPConn
	GetTCPConnection() *net.TCPConn
	// GetConnId 获取当前连接ID
	GetConnId() uint32
	// RemoteAddr 获取远程客户端地址信息
	RemoteAddr() net.Addr

	// 按照TLV格式的数据发送 使用者不能直接发送一个二进制的数据给用户了，需要按照TLV的格式发送
	// TLV:标识域（Tag）+长度域（Length）+值域（Value)

	// SendMsg 直接将Message数据发送给远程的TCP客户端(无缓冲)
	SendMsg(msgId uint32, data []byte) error

	// SendBuffMsg 直接将Message数据发送给远程的TCP客户端(有缓冲)
	SendBuffMsg(msgId uint32, data []byte) error


	// SetProperty 设置链接属性
	SetProperty(key string, value interface{})
	// GetProperty 获取链接属性
	GetProperty(key string) (interface{}, error)
	// RemoveProperty 移除链接属性
	RemoveProperty(key string)


}


// HandFunc 定义个同意的处理连接业务的接口
/**
	first param： socket 原生连接
	second param: 客户端请求的数据
	three param: 客户端请求的数据长度
 */
type HandFunc func(* net.TCPConn, []byte, int) error