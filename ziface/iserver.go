package ziface

type IServer interface {
	// Start 启动服务器方法
	Start()
	// Stop 停止服务器方法
	Stop()
	// Serve 开启服务器方法
	Serve()
	// AddRouter 路由功能：给当前服务注册一个路由业务方法，供了护短链接处理使用
	// AddRouter(router IRouter)
	AddRouter(msgId uint32, router IRouter)
	// GetConnMgr 得到链接管理
	GetConnMgr() IConnManager
	// SetOnConnStart 设置该Server的链接创建时Hook函数
	SetOnConnStart(func (connection IConnection))
	// SetOnConnStop 设置该Server的连接断开时的Hook函数
	SetOnConnStop(func (connection IConnection))
	// CallOnConnStart 调用链接OnConnStart Hook 函数
	CallOnConnStart(conn IConnection)
	// CallOnConnStop 调用链接OnConnStop Hook函数
	CallOnConnStop(conn IConnection)

}
